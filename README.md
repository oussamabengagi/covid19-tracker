A web-based collection of Covid-19 tools for schemes/algorithms used in


**This toolkit has been built with educational purposes in mind!**  
It is meant to play around with different schemes and algorithms to understand how they work.  
However, you must be **extremely careful** when using real/live/mainnet data/keys/credentials!  
A web browser usually is not a safe environment to either create strong cryptographic keys and/or
paste sensitive information into. So consider yourself warned.

[Live version](https://moneyaccounts.com/)

## Tools
* [Elliptic Curve Cryptography / Key Pair page](https://moneyaccounts.com/)
* [Hierarchical Deterministic Wallet page](https://guggero.github.io/cryptography-toolkit/#!/hd-wallet)
* [Bitcoin Block Parser page](https://guggero.github.io/cryptography-toolkit/#!/bitcoin-block)
* [Shamir's Secret Sharing Scheme page](https://guggero.github.io/cryptography-toolkit/#!/shamir-secret-sharing)
* [BIP Schnorr Signatures page](https://guggero.github.io/cryptography-toolkit/#!/schnorr)
* [MuSig: Key Aggregation for Schnorr Signatures page](https://moneyaccounts.com/)
* [Transaction Creator page](https://guggero.github.io/cryptography-toolkit/#!/transaction-creator)
* [aezeed Cipher Seed Scheme page](https://guggero.github.io/cryptography-toolkit/#!/aezeed)
* [Macaroons page](https://guggero.github.io/cryptography-toolkit/#!/macaroon)
* [Wallet Import helper page](https://moneyaccounts.com/)

## Send Thanks
